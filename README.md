# SQL Query
Silahkan masuk ke https://pma.mes.stechoq.com dengan :  
username : seleksi  
password : oR1L9hk4TBST3FBuJiIp2HPVXEWGbjyd  
Buka database "seleksi_db" dan masuk ke tabel "d_access_proposal".  
![image](/uploads/9c8fd16f40e3791ca01c193e8228d444/image.png)  

Dari tabel tersebut, silahkan buat query select untuk menampilkan :
1. Total status (Reject, Accept, Idle) untuk setiap mahasiswa, sehingga menampilkan data sebagai berikut :  
![image](/uploads/4ca5c43e8ac70a11f7651ab35045e59e/image.png)
2. Selisih tanggal antar proposedDate, sehingga menampilkan data sebagai berikut :  
![image](/uploads/e753f74f79dce587f56dcc42e30baf85/image.png)

# Javascript (Node.js)
1. Kelompokkan array berikut berdasarkan kelasnya
```json
[
    {
        "nama": "muham", 
        "kelas": 12
    }, {
        "nama": "yeremia", 
        "kelas": 13
    }, {
        "nama": "harish", 
        "kelas": 12
    }, {
        "nama": "febri", 
        "kelas": 14
    }
]
```
sehingga menjadi :
```json
{
    12: [
        {"nama": "muham", "kelas": 12},
        {"nama": "harish", "kelas": 12}
    ],
    13: [
        {"nama": "yeremia", "kelas": 13}
    ],
    14: [
        {"nama": "febri", "kelas": 14}
    ]
}
```
dengan menggunakan javascript !  

2. Buatlah program scheduling jam pelajaran sederhana yang akan membunyikan bel setiap ganti jam pelajaran, dengan ketentuan sebagai berikut
	- Bel masuk sekolah dimulai pada __pukul 07:00__
    - Lama Tiap Jam Pelajaran adalah __45 menit__
	- 1 Hari terdiri dari __8 Jam Pelajaran__
	- Terdapat __2 kali istirahat__ yg terjadi __setelah__ selesainya jam pelajaran ke-__3__ dan ke-__6__
	- Lama tiap istirahat adalah __15 menit__

Output bel bisa digantikan dengan console.log / print, yang akan menuliskan jenis bel (ganti jam pelajaran, istirahat mulai/selesai, bel masuk/pulang sekolah) serta waktu nya

Untuk contoh output akan seperti berikut :
```
07:00 - bel masuk - jam pelajaran 1
07:45 - bel pergantian jam pelajaran 2
08:30 - bel pergantian jam pelajaran 3
09:15 - bel istirahat mulai
09:30 - bel istirahat selesai / ganti jam pelajaran 4
10:15 - bel pergantian jam pelajaran 5
11:00 - bel pergantian jam pelajaran 6
11:45 - bel istirahat mulai
12:00 - bel istirahat selesai / ganti jam pelajaran 7
12:45 - bel pergantian jam pelajaran 8
13:30 - bel pulang
```

# Upload
Kumpulkan soal dalam bentuk pdf ke kolom chat zoom
